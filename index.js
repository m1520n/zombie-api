const api = require('./src/app');
const config = require('./src/config');

if (!process.env.API_ENV) {
  console.log('You need to specify the API_ENV environment variable!');
  return;
}
const PORT = config[process.env.API_ENV].port;

api.listen(PORT, () => {
  console.log(`Zombie API listening on: ${PORT}`);
});

module.exports = api;
