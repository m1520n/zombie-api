
**

## Zombie API

 **1. Environment setup**

Node 8.10.0 is required to run the API

Before the running the API you need to install all the dependencies by running `yarn` or `npm install`.

This API uses `MongoDB` as a data layer. You need to have `MongoDB` installed on your machine.

Mongo instance should be available at `127.0.0.1:27017`.

You need to pass `API_ENV` environment variables before running the API.

Available `API_ENV` values are `prod`, `stage`, `test` and `dev`.


**2. Available tasks**

`yarn start` - run the API on destination environment.

`yarn start:dev` - Used for development. This script will run static code analysis and reload the API after each code change.

`yarn lint` - Run ESLint static code analysis

`yarn lint:fix` - Run ESLint with automatic error fixing

`yarn test:unit` - Run unit tests


**3. API documentation**

This API returns responses in JSON format.

**Available endpoints:**

**ITEMS**

**GET /item**
Returns all items
```
{
	status: Boolean,
	items: [{
		_id: String,
		name: String,
		externalId: Number,
		zombieId: String,
	}]
}
```

**GET /item/:id/zombie**
Params: `id: String` - zombie id
Returns all items of a zombie
```
{
	status: Boolean,
	zombieId: String,
	zombieItems: {
		_id: String,
		name: String,
		externalId: Number,
		zombieId: String,
	}
}
```

**GET /item/:id**
Params: `id: String` - item id
Returns a single item
```
{
	status: Boolean,
	items: {
		_id: String,
		name: String,
		externalId: Number,
		zombieId: String,
	}
}
```

**POST /item**
Create item and associate with a zombie
Body:
```
{
	name: String,
	externalId: Number,
	zombieId: String
}
```

Response:
```
{
    status: Boolean,
    item: {
        _id: String,
        name: String,
        externalId: Number,
        zombieId: String
    }
}
```

**PATCH /item/:id**
Edit item. You can only edit owner of the item.
Params: `id: String` - item id
Body:
```
{
	zombieId: String
}
```

**DELETE /item/:id**
Removes item and disassociates it from a zombie
Params: `id: String` - item id

Response:
```
{
	status: Boolean
}
```

**ZOMBIES**

**GET /zombie**
Returns all zombies

Response:
```
{
	status: Boolean,
	zombies: [{
	    _id: "String,
		items: [id: String]
        name: String,
	}]
}
```

**GET /zombie/:id**
Returns single zombie with item details and values calculated to currencies

Params: `id: String` - zombie id

Response:
```
{
	status: Boolean,
	{
    zombie: {
         _id: String,
        name: String,
	    items: [{
	        _id: String,
	        name: String,
	        externalId: Number,
	        zombieId: String,
        }],
    },
    itemsTotal: {
        PLN: Number,
        EUR: Number,
        USD: Number
    }
}
```



**POST /zombie**
Create item and associate with a zombie
Body:
```
{
	name: String,
}
```

Response:
```
{
    status: Boolean,
    zombie: {
        _id: String,
        name: String,
        items: [],
    }
}
```


**PATCH /zombie/:id**
Edit zombie.
Params: `id: String` - item id
Body:
```
{
	name: String
}
```


**DELETE /zombie/:id**
Removes zombie and associated items
Params: `id: String` - zombie id

Response:
```
{
	status: Boolean
}
```