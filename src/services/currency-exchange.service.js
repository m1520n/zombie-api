const fetch = require('node-fetch');

const config = require('../config');

const currencyExchangeApiUri = config[process.env.API_ENV].currencyExchangeApiUri;

const CurrencyExchangeService = {
  fetchTodayExchangeRates: async () => {
    try {
      const responseToday = await fetch(`${currencyExchangeApiUri}/tables/C/today?format=json`, {
        headers: {
          Accept: 'application/json',
        }
      });

      const responseTodayText = await responseToday.text();

      if (responseTodayText.includes('404 NotFound')) {
        const latestAvailableResponse = await fetch(`${currencyExchangeApiUri}/tables/C?format=json`, {
          headers: {
            Accept: 'application/json',
          }
        });

        return JSON.parse(await latestAvailableResponse.text());
      }

      return JSON.parse(responseTodayText);
    } catch (error) {
      console.log(error);
    }
  },
};

module.exports = CurrencyExchangeService;
