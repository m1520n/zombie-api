const fetch = require('node-fetch');

const config = require('../config');

const itemExchangeApiUri = config[process.env.API_ENV].itemExchangeApiUri;

const ItemExchangeService = {
  fetchItemPrices: async () => {
    try {
      const response = await fetch(`${itemExchangeApiUri}/items`);
      return await response.json();
    } catch (error) {
      console.log(error);
    }
  },
};

module.exports = ItemExchangeService;
