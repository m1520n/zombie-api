const router = require('express-promise-router')();

const zombieRoutes = require('../components/zombie/zombie.routes');
const itemRoutes = require('../components/item/item.routes');

router.use('/zombie', zombieRoutes);
router.use('/item', itemRoutes);

module.exports = router;
