const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const arrayLimit = (val) => val.length <= 5;

const zombieSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  items: {
    type: [{
      type: Schema.Types.ObjectId,
      ref: 'Item'
    }],
    validate: [arrayLimit, 'Zombie can have maximum of 5 items!']
  },
});

module.exports = mongoose.model('Zombie', zombieSchema);
