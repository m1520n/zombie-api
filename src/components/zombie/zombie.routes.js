const router = require('express-promise-router')();

const {
  fetchZombies,
  fetchZombie,
  addZombie,
  editZombie,
  removeZombie,
} = require('./zombie.controller');

router.get('/', fetchZombies);
router.get('/:id', fetchZombie);

router.post('/', addZombie);

router.patch('/:id', editZombie);

router.delete('/:id', removeZombie);

module.exports = router;
