const {
  fetchZombies,
  fetchZombie,
  editZombie,
  addZombie,
  removeZombie,
} = require('./zombie.controller');

let res;

beforeEach(() => {
  jest.resetAllMocks();
  res = {
    send: jest.fn(),
    status: jest.fn(() => res),
    json: jest.fn(),
  };
});

describe('fetchZombies', () => {
  test('sends list of zombies', async () => {
    const zombies = [
      { id: 1 },
      { id: 2 },
      { id: 3 },
    ];
    const listZombies = jest.fn(() => Promise.resolve(zombies));
    await fetchZombies({}, res, null, { listZombies });

    expect(res.json).toHaveBeenCalledWith({
      status: true,
      zombies,
    });
  });
});

describe('fetchZombie', () => {
  test('sends zombie with item details and calculated values from cache if cache is available', async () => {
    const id = 'ZOMBIE_ID';
    const zombie = {
      _id: 'ZOMBIE_ID',
      name: 'ZOMBIE_NAME',
      items: [
        {
          externalId: 1,
          name: 'ITEM_NAME_1',
          id: 'ITEM_ID_1'
        },
        {
          externalId: 2,
          name: 'ITEM_NAME_2',
          id: 'ITEM_ID_2'
        },
      ],
    };

    const getFromCache = () => Promise.resolve(({
      items: [
        {
          name: 'ITEM_NAME_1',
          id: 1,
          price: 200,
        },
        {
          name: 'ITEM_NAME_2',
          id: 2,
          price: 400,
        }
      ],
      timestamp: Date.now() + 10000,
    }));
    const setInCache = jest.fn(() => Promise.resolve());
    const getZombie = jest.fn(() => Promise.resolve(zombie));
    const fetchTodayExchangeRates = jest.fn(() => Promise.resolve([
      { rates: [
          { code: 'USD', ask: 3.86 },
          { code: 'EUR', ask: 4.89 },
        ]
      }
    ]));
    const fetchItemPrices = jest.fn(() => Promise.resolve());

    await fetchZombie({ params: { id } }, res, null, {
      getZombie,
      getFromCache,
      setInCache,
      fetchTodayExchangeRates,
    });

    expect(fetchItemPrices).not.toHaveBeenCalled();
    expect(getZombie).toHaveBeenCalledWith(id);
    expect(res.json).toHaveBeenCalledWith({
      status: true,
      itemsTotal: {
        EUR: 2934,
        PLN: 600,
        USD: 2316,
      },
      zombie: {
        _id: 'ZOMBIE_ID',
        name: 'ZOMBIE_NAME',
        items: [
          {
            externalId: 1,
            id: 'ITEM_ID_1',
            name: 'ITEM_NAME_1',
          },
          {
            externalId: 2,
            id: 'ITEM_ID_2',
            name: 'ITEM_NAME_2',
          },
        ],
      },
    });
  });

  test('sends zombie with item details and calculated values from api if cache is expired', async () => {
    const id = 'ZOMBIE_ID';
    const zombie = {
      _id: 'ZOMBIE_ID',
      name: 'ZOMBIE_NAME',
      items: [
        {
          externalId: 1,
          name: 'ITEM_NAME_1',
          id: 'ITEM_ID_1'
        },
        {
          externalId: 2,
          name: 'ITEM_NAME_2',
          id: 'ITEM_ID_2'
        },
      ],
    };

    const getFromCache = () => Promise.resolve(({
      items: [
        {
          name: 'ITEM_NAME_1',
          id: 1,
          price: 200,
        },
        {
          name: 'ITEM_NAME_2',
          id: 2,
          price: 400,
        }
      ],
      timestamp: Date.now() - 86400000,
    }));

    const fetchItemPrices = jest.fn(() => Promise.resolve({
      items: [
        {
          name: 'ITEM_NAME_1',
          id: 1,
          price: 300,
        },
        {
          name: 'ITEM_NAME_2',
          id: 2,
          price: 200,
        }
      ],
    }));

    const setInCache = jest.fn(() => Promise.resolve());
    const getZombie = jest.fn(() => Promise.resolve(zombie));
    const fetchTodayExchangeRates = jest.fn(() => Promise.resolve([
      { rates: [
          { code: 'USD', ask: 3.92 },
          { code: 'EUR', ask: 4.99 },
        ]
      }
    ]));

    await fetchZombie({ params: { id } }, res, null, {
      getZombie,
      getFromCache,
      setInCache,
      fetchTodayExchangeRates,
      fetchItemPrices,
    });

    expect(fetchItemPrices).toHaveBeenCalled();
    expect(getZombie).toHaveBeenCalledWith(id);
    expect(res.json).toHaveBeenCalledWith({
      status: true,
      itemsTotal: {
        EUR: 2495,
        PLN: 500,
        USD: 1960,
      },
      zombie: {
        _id: 'ZOMBIE_ID',
        name: 'ZOMBIE_NAME',
        items: [
          {
            externalId: 1,
            id: 'ITEM_ID_1',
            name: 'ITEM_NAME_1',
          },
          {
            externalId: 2,
            id: 'ITEM_ID_2',
            name: 'ITEM_NAME_2',
          },
        ],
      },
    });
  });
});

// TODO: add  missing unit tests