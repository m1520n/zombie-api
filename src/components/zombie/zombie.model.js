
const ZombieSchema = require('./zombie.schema');

const defaultSchema = {
  Zombie: ZombieSchema,
};

const ZombieModel = {
  listZombies: async (schema = defaultSchema) => {
    try {
      return await schema.Zombie.find({});
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },

  getZombie: async (id, schema = defaultSchema) => {
    try {
      return await schema.Zombie.findById(id).populate('items');
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },

  createZombie: async (body, schema = defaultSchema) => {
    try {
      const zombie = new schema.Zombie(body);
      await zombie.save();
      return zombie;
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },

  updateZombie: async ({ id, body }, schema = defaultSchema) => {
    try {
      return await schema.Zombie.findOneAndUpdate({ _id: id }, body, { new: true });
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },
  deleteZombie: async (id, schema = defaultSchema) => {
    try {
      const zombie = await schema.Zombie.findById(id);
      await zombie.remove();
      // TODO: remove items association
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },
};

module.exports = ZombieModel;
