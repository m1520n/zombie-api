const { times } = require ('number-precision');

const { getFromCache, setInCache } = require('../../utils/cache');
const ZombieModel = require('./zombie.model');
const CurrencyExchangeService = require('../../services/currency-exchange.service');
const ItemExchangeService = require('../../services/item-exchange.service');

const defaultDependencies = {
  ...ZombieModel,
  ...CurrencyExchangeService,
  ...ItemExchangeService,
  getFromCache,
  setInCache,
};

const ZombieController = {
  fetchZombies: async (req, res, next, dependencies = defaultDependencies) => {
    const zombies = await dependencies.listZombies();

    res.json({
      status: true,
      zombies,
    });
  },

  fetchZombie: async (req, res, next, dependencies = defaultDependencies) => {
    const { params } = req;
    const { id } = params;

    const zombie = await dependencies.getZombie(id);

    if(!zombie) {
      res
        .status(404)
        .json({ status: false, message: 'Zombie not found' });

      return;
    }

    const now = Date.now();
    const dayInMili = 86400000;

    let itemPrices;

    const cachedItemPrices = await dependencies.getFromCache('itemPrices');

    if (cachedItemPrices && ((cachedItemPrices.timestamp + dayInMili) > now)) {
      itemPrices = cachedItemPrices;
    } else {
      itemPrices = await dependencies.fetchItemPrices();
    }

    await dependencies.setInCache({
      key: 'itemPrices',
      value: itemPrices,
    });

    const [{ rates }] = await dependencies.fetchTodayExchangeRates();

    const exchangeRatesDict = {};
    const itemPricesDict = {};

    rates.forEach(({ code, ask }) => {
      exchangeRatesDict[code] = ask;
    });

    itemPrices.items.forEach(({ id, price }) => {
      itemPricesDict[id] = price;
    });

    const itemsTotalPLN = zombie.items
      .map(({ externalId }) => itemPricesDict[externalId])
      .reduce((prev, curr) => prev + curr, 0);

    const itemsTotal = {
      PLN: itemsTotalPLN,
      EUR: times(itemsTotalPLN, exchangeRatesDict['EUR']),
      USD: times(itemsTotalPLN, exchangeRatesDict['USD']),
    };

    res.json({
      status: true,
      zombie,
      itemsTotal,
    });
  },

  addZombie: async (req, res, next, dependencies = defaultDependencies) => {
    const { body } = req;
    // TODO: fix error handling for duplicate zombies
    const zombie = await dependencies.createZombie({
      ...body,
      created: Date.now(),
    });

    res.json({
      status: true,
      zombie,
    });
  },

  editZombie: async (req, res, next, dependencies = defaultDependencies) => {
    const { body, params } = req;
    const { id } = params;

    // TODO: add check if zombie exists

    const zombie = await dependencies.updateZombie({ id, body });

    res.json({
      status: true,
      zombie,
    });
  },

  removeZombie: async (req, res, next, dependencies = defaultDependencies) => {
    const { params } = req;
    const { id } = params;

    // TODO: add check if zombie exists

    await dependencies.deleteZombie(id);

    res.json({
      status: true,
    });
  },
};

module.exports = ZombieController;
