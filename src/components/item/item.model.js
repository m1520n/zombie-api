
const ItemSchema = require('./item.schema');
const ZombieSchema = require('../zombie/zombie.schema');

const defaultSchemas = {
  Item: ItemSchema,
  Zombie: ZombieSchema,
};

const ItemModel = {
  listItems: async (zombieId = null, schemas = defaultSchemas) => {
    try {
      return await schemas.Item.find(zombieId ? { zombieId } : {});
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },

  getItem: async (id, schemas = defaultSchemas) => {
    try {
      return await schemas.Item.findById(id);
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },

  addZombieItem: async (body, schemas = defaultSchemas) => {
    // TODO: add checking if zombie and item id and name exists
    const { zombieId } = body;
    const zombie = await schemas.Zombie.findById(zombieId);

    if (!zombie) {
      throw new Error('Zombie not found');
    }

    const item = new schemas.Item(body);

    await item.save();

    zombie.items.push(item);
    await zombie.save();

    return item;
  },

  updateItem: async ({ id, body }, schemas = defaultSchemas) => {
    try {
      const newZombie = body.zombieId;

      const item = await schemas.Item.findById(id);
      const zombie = await schemas.Zombie.findById(zombieId);
      const updatedItem = await schemas.Item.findOneAndUpdate({ _id: id }, body, { new: true });

      zombie.items.pull(item);
      await zombie.save();

      newZombie.items.push(item);
      await newZombie.save();

      return updatedItem;
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },

  deleteItem: async (id, schemas = defaultSchemas) => {
    try {
      const item = await schemas.Item.findById(id);
      // TODO: handle not found
      await item.remove();
      const zombie = await schemas.Zombie.findById(item.zombieId);
      zombie.items.pull(item);
      await zombie.save();
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  },
};

module.exports = ItemModel;
