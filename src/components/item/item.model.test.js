const {
  listItems,
  getItem,
  addZombieItem,
  updateItem,
  deleteItem,
} = require('./item.model');

beforeEach(() => {
  jest.resetAllMocks();
});

const itemFindMock = jest.fn(() => Promise.resolve());
const zombieFindMock = jest.fn(() => Promise.resolve());
const zombieFindByIdMock = jest.fn(() => Promise.resolve({ name: 'ZOMBIE_NAME', items: []}));

class Zombie {
  static get find() {
    return zombieFindMock;
  }

  static get findById() {
    return zombieFindByIdMock;
  }

  save() {
    return jest.fn(Promise.resolve());
  }
};

class Item {
  static get find() {
    return itemFindMock;
  }

  save() {
    return jest.fn(Promise.resolve());
  }
};

describe('listItems', () => {
  test('fetches all items if no param is', async () => {
    await listItems(undefined, { Item });

    expect(Item.find).toHaveBeenCalledWith({});
  });

  test('fetches all items for zombie if zombieId is passed', async () => {
    const items = [
      { id: 1 },
      { id: 2 },
      { id: 3 },
    ];

    await listItems('ZOMBIE_ID', { Item });

    expect(Item.find).toHaveBeenCalledWith({ zombieId: 'ZOMBIE_ID' });
  });
});

// TODO: add missing unit tests
