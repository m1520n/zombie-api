const router = require('express-promise-router')();

const {
  fetchAllItems,
  fetchZombieItems,
  fetchItem,
  addItem,
  editItem,
  removeItem,
} = require('./item.controller');

router.get('/', fetchAllItems);
router.get('/:id/zombie', fetchZombieItems);
router.get('/:id', fetchItem);

router.post('/', addItem);

router.patch('/:id', editItem);

router.delete('/:id', removeItem);

module.exports = router;
