const {
  fetchAllItems,
  fetchZombieItems,
  fetchItem,
  addItem,
  editItem,
  removeItem,
} = require('./item.controller');

let res;

beforeEach(() => {
  jest.resetAllMocks();
  res = {
    send: jest.fn(),
    status: jest.fn(() => res),
    json: jest.fn(),
  };
});

describe('fetchAllItems', () => {
  test('sends list of items', async () => {
    const items = [
      { id: 1 },
      { id: 2 },
      { id: 3 },
    ];
    const listItems = jest.fn(() => Promise.resolve(items));
    await fetchAllItems({}, res, null, { listItems });

    expect(res.json).toHaveBeenCalledWith({
      status: true,
      items,
    });
  });
});

describe('fetchZombieItems', () => {
  test('sends list of items of a zombie', async () => {
    const zombieItems = [
      { id: 1 },
      { id: 3 },
    ];
    const id = 'ZOMBIE_ID';
    const listItems = jest.fn(() => Promise.resolve(zombieItems));
    await fetchZombieItems({ params: { id } }, res, null, { listItems });

    expect(listItems).toHaveBeenCalledWith(id);
    expect(res.json).toHaveBeenCalledWith({
      status: true,
      zombieId: id,
      zombieItems,
    });
  });
});

describe('fetchItem', () => {
  test('sends item', async () => {
    const item = { id: 1 };
    const id = 'ITEM_ID';
    const getItem = jest.fn(() => Promise.resolve(item));
    await fetchItem({ params: { id } }, res, null, { getItem });

    expect(getItem).toHaveBeenCalledWith(id);
    expect(res.json).toHaveBeenCalledWith({
      status: true,
      item,
    });
  });
});

describe('addItem', () => {
  test('creates new item', async () => {
    const body = {
      externalId: 1,
      name: 'ITEM_NAME',
      zombieId: 'ZOMBIE_ID',
    };

    const createdItem = {
      _id: 'ITEM_ID',
      name: 'ITEM_NAME',
      externalId: 1,
      zombieId: 'ZOMBIE_ID',
    };

    const addZombieItem = jest.fn(() => Promise.resolve(createdItem));
    await addItem({ body }, res, null, { addZombieItem });

    expect(addZombieItem).toHaveBeenCalledWith({
      ...body,
      created: expect.any(Number),
    });

    expect(res.json).toHaveBeenCalledWith({
      status: true,
      item: createdItem,
    });
  });
});

describe('editItem', () => {
  test('updates item', async () => {
    const params = {
      id: 'NEW_ZOMBIE_ID',
    };

    const body = {
      zombieId: 'ZOMBIE_ID',
    };

    const updatedItem = {
      _id: 'ITEM_ID',
      name: 'ITEM_NAME',
      externalId: 1,
      zombieId: 'NEW_ZOMBIE_ID',
    };

    const updateItem = jest.fn(() => Promise.resolve(updatedItem));
    await editItem({ body, params }, res, null, { updateItem });

    expect(updateItem).toHaveBeenCalledWith(params.id, body);

    expect(res.json).toHaveBeenCalledWith({
      status: true,
      item: updatedItem,
    });
  });
});

describe('removeItem', () => {
  test('removes item', async () => {
    const params = {
      id: 'ITEM_ID',
    };

    const deleteItem = jest.fn(() => Promise.resolve());
    await removeItem({ params }, res, null, { deleteItem });

    expect(deleteItem).toHaveBeenCalledWith(params.id);

    expect(res.json).toHaveBeenCalledWith({
      status: true,
    });
  });
});
