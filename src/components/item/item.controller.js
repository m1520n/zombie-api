const ItemModel = require('./item.model');

const defaultDependencies = {
  ...ItemModel,
};

const ItemController = {
  fetchAllItems: async (req, res, next, dependencies = defaultDependencies) => {
    const allItems = await dependencies.listItems();

    res.json({
      status: true,
      items: allItems,
    });
  },

  fetchZombieItems: async (req, res, next, dependencies = defaultDependencies) => {
    const { params } = req;
    const { id } = params;

    const zombieItems = await dependencies.listItems(id);

    res.json({
      status: true,
      zombieId: id,
      zombieItems,
    });
  },

  fetchItem: async (req, res, next, dependencies = defaultDependencies) => {
    const { params } = req;
    const { id } = params;

    const item = await dependencies.getItem(id);

    if(!item) {
      res
        .status(404)
        .json({ status: false, message: 'Item not found' });

      return;
    }

    res.json({
      status: true,
      item,
    });
  },

  addItem: async (req, res, next, dependencies = defaultDependencies) => {
    const { body } = req;
    // TODO: fix error handling for duplicate items

    let item;

    try {
      item = await dependencies.addZombieItem({
        ...body,
        created: Date.now(),
      });
    } catch (error) {
      if (error.message === 'Zombie not found') {
        res
          .status(404)
          .json({
            status: false,
            message: 'Zombie not found',
        });

        return;
      }

      if (error.errors && error.errors.items) {
        res
          .status(500)
          .json({
            status: false,
            message: error.errors.items.message,
          });

        return;
      }

      res
        .status(500)
        .json({
          status: false,
          message: 'INTERNAL_SERVER_ERROR',
        });
    }

    res.json({
      status: true,
      item,
    });
  },

  editItem: async (req, res, next, dependencies = defaultDependencies) => {
    const { body, params } = req;
    const { id } = params;

    // TODO: add check if zombie exists

    const item = await dependencies.updateItem(id, body);

    res.json({
      status: true,
      item,
    });
  },

  removeItem: async (req, res, next, dependencies = defaultDependencies) => {
    const { params } = req;
    const { id } = params;

    // TODO: add check if zombie exists

    await dependencies.deleteItem(id);

    res.json({
      status: true,
    });
  },
};

module.exports = ItemController;
