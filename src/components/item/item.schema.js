const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const itemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    // TODO: add available names
    // TODO: add projection to exclude __v
  },
  zombieId: {
    type: Schema.Types.ObjectId,
    ref: 'Zombie',
    required: true,
  },
  externalId: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model('Item', itemSchema);
