const statusCodes = require('../utils/status-codes');

const errorHandlerMiddleware = (err, req, res, next) => {
  if (!process.env.DISABLE_LOGS) {
    console.log(err);
  }

  if (res.headersSent) {
    return next(err);
  }

  const { statusCode, customMessage } = err;

  if (statusCode && customMessage) {
    return res.status(statusCode).send({
      status: false,
      error: customMessage,
    });
  }

  return res
    .status(statusCodes.INTERNAL_SERVER_ERROR)
    .send({
      status: false,
      error: 'INTERNAL SERVER ERROR',
    });
};

module.exports = errorHandlerMiddleware;
