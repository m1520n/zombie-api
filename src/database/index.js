const mongoose = require('mongoose');
mongoose.Promise = Promise;

const config = require('../config');

const DB_URI = config[process.env.API_ENV].databaseUri;

mongoose.connect(`mongodb://${DB_URI}`, {
  useNewUrlParser: true,
  useCreateIndex: true,
});

mongoose.connection.on('connected', () => {
  console.log(`Mongoose default connection open to ${DB_URI}`);
});
