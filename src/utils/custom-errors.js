class GenericError extends Error {
  constructor({
    error = new Error().stack,
    message = 'INTERNAL SERVER ERROR',
    statusCode = 500,
  }) {
    super(message);
    this.customMessage = message;
    this.message = error.stack || message;
    this.name = this.constructor.name;
    this.statusCode = statusCode;
  }
}

class AuthorisationError extends GenericError {
  constructor({
    message = 'Authorisation Error',
    statusCode = 403,
    error,
  }) {
    super({ message, statusCode, error });
    this.name = this.constructor.name;
  }
}

class BadRequestError extends GenericError {
  constructor({
    message = 'Bad Request Error',
    statusCode = 400,
    error,
  }) {
    super({ message, statusCode, error });
    this.name = this.constructor.name;
  }
}

module.exports = {
  AuthorisationError,
  BadRequestError,
  GenericError,
};

