const NodeCache = require('node-cache');

const zombieCache = new NodeCache();

const getFromCache = key => new Promise((resolve, reject) => {
  zombieCache.get(key, (err, value) => {
    if (err) {
      reject(err);
    }
    resolve(value);
  });
});

const setInCache = ({ key, ttl, value }) => new Promise((resolve, reject) => {
  zombieCache.set(key, value, ttl, (err, success) => {
    if (err || !success) {
      reject(err);
    }
    resolve();
  });
});

module.exports = {
  getFromCache,
  setInCache,
};
