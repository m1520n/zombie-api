require('dotenv').config();
const express = require('express');
const logger = require('morgan');
const createError = require('http-errors');
const cors = require('cors');
const app = express();
const NodeCache = require( "node-cache" );

const zombieCache = new NodeCache();

const router = require('./routes');
const errorHandlerMiddleware = require('./middleware/error-middleware');
require('./database');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(logger('dev'));
app.use('/', router);

// return Method Not Allowed if no route
app.all('*', (req, res) => {
  res.sendStatus(405);
});

app.use(errorHandlerMiddleware);

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // return error
  res.status(err.status || 500);
  res.json({
    status: err.status,
  });
});

module.exports = app;
