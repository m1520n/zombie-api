const config = {
  dev: {
    port: 3001,
    databaseUri: 'localhost:27017/zombie-dev',
    itemExchangeApiUri: 'https://zombie-items-api.herokuapp.com/api',
    currencyExchangeApiUri: 'http://api.nbp.pl/api/exchangerates',
  },
  stage: {
    port: 3002,
    databaseUri: 'localhost:27017/zombie-stage',
    itemExchangeApiUri: 'https://zombie-items-api.herokuapp.com/api',
    currencyExchangeApiUri: 'http://api.nbp.pl/api/exchangerates',
  },
  test: {
    port: 3003,
    databaseUri: 'localhost:27017/zombie-test',
    itemExchangeApiUri: 'https://zombie-items-api.herokuapp.com/api',
    currencyExchangeApiUri: 'http://api.nbp.pl/api/exchangerates',
  },
  prod: {
    port: 3004,
    databaseUri: 'localhost:27017/zombie-prod',
    itemExchangeApiUri: 'https://zombie-items-api.herokuapp.com/api',
    currencyExchangeApiUri: 'http://api.nbp.pl/api/exchangerates',
  },
};

module.exports = config;
