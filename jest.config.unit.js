const path = require('path');

module.exports = {
  testEnvironment: 'node',
  rootDir: './src',
  testURL: 'http://localhost/',
};
